package gateway.v2;

import gateway.Gateway;
import gateway.GatewayStatus;

public class OpenStatus implements GatewayStatus {
    @Override
    public void pass(final Gateway p) {
        p.close();
        p.setClosed();
    }

    @Override
    public void coin(final Gateway p) {
        p.pay();
    }
}
