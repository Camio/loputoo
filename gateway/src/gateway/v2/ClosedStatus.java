package gateway.v2;

import gateway.Gateway;
import gateway.GatewayStatus;

public class ClosedStatus implements GatewayStatus {
    @Override
    public void pass(final Gateway p) {
        p.alarm();
    }

    @Override
    public void coin(final Gateway p) {
        p.open();
        p.setOpen();
    }
}
