package gateway.v2;

import gateway.GatewayStatus;

import java.util.Observable;

public class GatewayWithObserver extends Observable implements gateway.Gateway {

    private GatewayStatus status;

    public GatewayWithObserver(){
        setClosed();
    }

    public void open(){
        System.out.println("Gate open.");
    }

    public void close(){
        System.out.println("Gate closed.");
    }

    public void setOpen(){
        setChanged();
        status = new OpenStatus();
    }

    public void setClosed(){
        setChanged();
        status = new ClosedStatus();
    }

    public void pay(){
        notifyObservers("Payment taken.");
    }

    @Override
    public void alarm() {
        notifyObservers("ALARM!!!");
    }

    public void pass() {
        status.pass(this);
    }

    public void coin() {
        status.coin(this);
    }
}
