package gateway;

public interface Gateway {
    void pass();

    void coin();

    void open();

    void close();

    void setOpen();

    void setClosed();

    void pay();

    void alarm();
}
