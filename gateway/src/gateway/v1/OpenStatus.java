package gateway.v1;

import gateway.Gateway;
import gateway.GatewayStatus;

public class OpenStatus implements GatewayStatus {
    @Override
    public void pass(Gateway p) {
        p.close();
        p.setClosed();
    }

    @Override
    public void coin(Gateway p) {
        p.pay();
    }
}
