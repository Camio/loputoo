package gateway.v1;

import gateway.GatewayStatus;

public class GatewayImpl implements gateway.Gateway {

    private GatewayStatus status = new ClosedStatus();

    //ava
    public void open(){
        System.out.println("Gate open.");
    }

    //sule
    public void close(){
        System.out.println("Gate closed.");
    }

    //määra avatuks
    public void setOpen(){
        status = new OpenStatus();
    }

    //määra suletuks
    public void setClosed(){
        status = new ClosedStatus();
    }

    //täna
    public void pay(){
        System.out.println("Payment taken.");
    }

    public void alarm(){
        System.out.println("ALARM!!!");
    }

    //mööda
    public void pass() {
        status.pass(this);
    }

    //münt
    public void coin() {
        status.coin(this);
    }
}
