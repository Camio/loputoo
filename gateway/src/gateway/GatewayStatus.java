package gateway;

public interface GatewayStatus {
    void pass(final Gateway p);
    void coin(final Gateway p);
}
