import gateway.Gateway;

public class Main {

    private static void start(Gateway gate) {
        gate.pass();
        gate.coin();
        gate.coin();
        gate.pass();
        gate.pass();
    }
}
