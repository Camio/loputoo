package Serpinski;

import Geometry.Point;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class SierpinskiTriangle implements Iterable {
    private List<Point> points;

    public SierpinskiTriangle(final Point a, final Point b, final Point c){
        this.points = Arrays.asList(a, b, c);
    }

    @Override
    public Iterator<? extends Point> iterator() {
        return points.iterator();
    }
}
