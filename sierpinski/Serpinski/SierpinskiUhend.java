package Serpinski;

import Geometry.Point;
import com.google.common.collect.Iterators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class SierpinskiUhend extends SierpinskiTriangle {

    private final List<SierpinskiTriangle> triangles = new ArrayList<>();

    public SierpinskiUhend(final Point a, final Point b, final Point c, final int degree){
        super(a, b, c);
        if (degree > 0){
            this.triangles.add(new SierpinskiUhend(a, a.getMidpoint(b), a.getMidpoint(c), degree-1));
            this.triangles.add(new SierpinskiUhend(b, a.getMidpoint(b), b.getMidpoint(c), degree-1));
            this.triangles.add(new SierpinskiUhend(c, c.getMidpoint(b), a.getMidpoint(c), degree-1));
        }
    }

    @Override
    public Iterator<? extends Point> iterator() {
        final List<Iterator> iteratorList = triangles.stream()
                .map(SierpinskiTriangle::iterator)
                .collect(Collectors.toList());
        iteratorList.add(super.iterator());
        return Iterators.concat(iteratorList.toArray(new Iterator[iteratorList.size()]));
    }
}
